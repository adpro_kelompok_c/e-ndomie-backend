package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieGorengJumbo extends CustomNoodle {
	public IndomieGorengJumbo() {
		description = "Indomie Goreng Polos Jumbo";
	}

	@Override
	public int cost() {
		return 10000;
	}
}
