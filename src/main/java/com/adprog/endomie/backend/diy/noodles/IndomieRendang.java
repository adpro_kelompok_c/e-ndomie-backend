package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieRendang extends CustomNoodle {
	public IndomieRendang() {
		description = "Indomie Goreng Rasa Rendang";
	}

	@Override
	public int cost() {
		return 7500;
	}
}
