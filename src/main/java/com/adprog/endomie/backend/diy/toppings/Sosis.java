package com.adprog.endomie.backend.diy.toppings;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class Sosis extends Topping {

	public Sosis(CustomNoodle customNoodle) {
		this.customNoodle = customNoodle;
	}

	@Override
	public String getDescription() {
		return customNoodle.getDescription() + " + Sosis";
	}

	@Override
	public int cost() {
		return 1000 + customNoodle.cost();
	}
}
