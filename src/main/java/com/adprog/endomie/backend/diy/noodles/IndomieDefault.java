package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieDefault extends CustomNoodle {
	public IndomieDefault() {
		description = "";
	}

	@Override
	public int cost() {
		return 0;
	}
}
