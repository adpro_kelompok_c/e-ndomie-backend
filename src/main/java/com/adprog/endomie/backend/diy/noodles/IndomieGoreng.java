package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;
public class IndomieGoreng extends CustomNoodle {
	public IndomieGoreng() {
		description = "Indomie Goreng Polos";
	}

	@Override
	public int cost() {
		return 7000;
	}
}
