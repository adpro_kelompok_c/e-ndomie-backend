package com.adprog.endomie.backend.diy.toppings;

import com.adprog.endomie.backend.diy.CustomNoodle;

public abstract class Topping extends CustomNoodle {
	CustomNoodle customNoodle;

	public abstract String getDescription();
}
