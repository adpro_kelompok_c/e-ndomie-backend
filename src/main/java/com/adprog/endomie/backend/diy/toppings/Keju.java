package com.adprog.endomie.backend.diy.toppings;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class Keju extends Topping {

	public Keju(CustomNoodle customNoodle) {
		this.customNoodle = customNoodle;
	}

	@Override
	public String getDescription() {
		return customNoodle.getDescription() + " + Keju";
	}

	@Override
	public int cost() {
		return 2000 + customNoodle.cost();
	}
}
