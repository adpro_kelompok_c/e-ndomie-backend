package com.adprog.endomie.backend.diy.toppings;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class Kornet extends Topping {

	public Kornet(CustomNoodle customNoodle) {
		this.customNoodle = customNoodle;
	}

	@Override
	public String getDescription() {
		return customNoodle.getDescription() + " + Kornet";
	}

	@Override
	public int cost() {
		return 1500 + customNoodle.cost();
	}
}
