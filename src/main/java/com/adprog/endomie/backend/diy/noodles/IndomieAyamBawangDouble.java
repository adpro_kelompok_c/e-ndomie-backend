package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieAyamBawangDouble extends CustomNoodle {
	public IndomieAyamBawangDouble() {
		description = "Indomie Kuah Rasa Ayam Bawang Double";
	}

	@Override
	public int cost() {
		return 12000;
	}
}
