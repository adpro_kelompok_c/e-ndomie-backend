package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieSoto extends CustomNoodle {
	public IndomieSoto() {
		description = "Indomie Kuah Rasa Soto";
	}

	@Override
	public int cost() {
		return 7000;
	}
}
