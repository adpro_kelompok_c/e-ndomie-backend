package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieRendangDouble extends CustomNoodle {
	public IndomieRendangDouble() {
		description = "Indomie Goreng Rasa Rendang Double";
	}

	@Override
	public int cost() {
		return 13000;
	}
}
