package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieKariAyamDouble extends CustomNoodle {
	public IndomieKariAyamDouble() {
		description = "Indomie Kuah Rasa Kari Ayam Double";
	}

	@Override
	public int cost() {
		return 12000;
	}
}
