package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieGorengDouble extends CustomNoodle {
	public IndomieGorengDouble() {
		description = "Indomie Goreng Polos Double";
	}

	@Override
	public int cost() {
		return 12000;
	}
}
