package com.adprog.endomie.backend.diy.toppings;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class Telur extends Topping {

	public Telur(CustomNoodle customNoodle) {
		this.customNoodle = customNoodle;
	}

	@Override
	public String getDescription() {
		return customNoodle.getDescription() + " + Telur";
	}

	@Override
	public int cost() {
		return 2000 + customNoodle.cost();
	}
}
