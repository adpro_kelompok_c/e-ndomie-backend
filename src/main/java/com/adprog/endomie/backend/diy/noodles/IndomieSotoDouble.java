package com.adprog.endomie.backend.diy.noodles;

import com.adprog.endomie.backend.diy.CustomNoodle;

public class IndomieSotoDouble extends CustomNoodle {
	public IndomieSotoDouble() {
		description = "Indomie Kuah Rasa Soto Double";
	}

	@Override
	public int cost() {
		return 12000;
	}
}
